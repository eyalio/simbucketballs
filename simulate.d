import std.stdio : writeln;
import std.getopt : getopt, config, defaultGetoptPrinter;
import std.variant : Algebraic, visit;
import std.random : uniform;
import std.array : array, appender;
import std.format : formattedWrite;
import std.algorithm : sum, map, filter, sort;
import std.typecons : Tuple;
import std.format : format;

/** Command-line options */
struct Opts {
    uint buckets;
    uint balls;
}

alias Null = typeof(null);
alias Maybe(T) = Algebraic!(Opts, Null);

auto parseOpts(string[] args)
{
    alias MOpts = Maybe!(Opts);
    Opts opts;
    auto helpInfo =
        getopt(args,
               config.required, "buckets", &opts.buckets,
               config.required, "balls", &opts.balls);
    if (helpInfo.helpWanted) {
        defaultGetoptPrinter(
            "Simulate random bucket filling",
            helpInfo.options);

        return MOpts(null);
    }
    return MOpts(opts);
}

struct Bucket {
    uint count;
}

struct Histogram {
    uint[uint] counts;
    auto pairs() const pure {
        alias Pair = Tuple!(uint, "size", uint, "count");
        return counts
            .byKeyValue
            .map!(pair => Pair(pair.key, pair.value))
            .array
            .sort!((a, b) => (a.size < b.size));
    }
    auto toString() const pure {
        auto writer = appender!string();
        foreach (pair; pairs) {
            formattedWrite(writer, "%s => %s\n", pair.size, pair.count);
        }
        return writer.data;
    }
}

struct Simulator {
    Opts opts;
    Bucket[] buckets;
    void simulate() {
        buckets = new Bucket[opts.buckets];
        foreach (i ; 0..opts.balls) {
            auto r = uniform(0, buckets.length);
            buckets[r].count++;
        }
    }
    Histogram histogram() {
        Histogram h;
        foreach (bucket ; buckets) {
            h.counts[bucket.count] = 1 + h.counts.get(bucket.count, 0);
        }
        return h;
    }
}

void main(string[] args)
{
    auto opts = parseOpts(args);
    opts.visit!(
        delegate(Null _) { writeln("Done"); },
        delegate(Opts opts) {
            auto simulator = Simulator(opts);
            simulator.simulate();
            auto h = simulator.histogram();
            writeln(h);
            auto pairs = h.pairs.filter!(pair => pair.size != 0);
            auto avg_misses = pairs
                .map!(pair => pair.count * pair.size *
                      (1 + real(0.5) + pair.size / real(2)))
                .sum / opts.balls;
            writeln("Average number of misses: ", avg_misses);

            auto total = pairs
                .map!(pair => pair.count)
                .sum;
            foreach (pair; pairs) {
                auto ratio = real(pair.count) / total;
                writeln(pair.size, " => %02.2f%%".format(100 * ratio));
            }
        });
}
